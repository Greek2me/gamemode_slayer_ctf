new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "CTF";
	title = "Flag Captures to Win";
	defaultValue = 0;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.CTF_flagReturnsToWin";
	type = "int";
	int_minValue = 0;
	int_maxValue = 100;
	guiTag = "rules slayer_ctf mode";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "CTF";
	title = "Flag Return Location";
	defaultValue = 0;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.CTF_flagReturnOnlyAtReturnBrick";
	type = "list";
	list_items = "0 Flag Spawn or Flag Return Point" NL "1 Flag Return Point Only";
	guiTag = "rules slayer_ctf mode";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "CTF";
	title = "Return Flag Without Own";
	defaultValue = 0;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.CTF_returnWithoutOwn";
	type = "bool";
	guiTag = "rules slayer_ctf mode";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "CTF";
	title = "Neutral Flags";
	defaultValue = 1;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.CTF_neutralFlags";
	type = "list";
	list_items = "0 Disabled" NL "1 Enabled" NL "2 Enabled, Neutral Flags Only";
	guiTag = "rules slayer_ctf mode";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "CTF";
	title = "Flag Return";
	defaultValue = 20;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.CTF_points_Flag";
	type = "int";
	int_minValue = -999;
	int_maxValue = 999;
	guiTag = "rules slayer_ctf points";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "CTF";
	title = "Flag Recovery";
	defaultValue = 10;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.CTF_points_FlagRecovery";
	type = "int";
	int_minValue = -999;
	int_maxValue = 999;
	guiTag = "rules slayer_ctf points";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "CTF";
	title = "Flag Recovery Mode";
	defaultValue = 2;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.CTF_flagRecovery";
	type = "list";
	list_items = "0 Disabled" NL "1 Instant Recovery" NL "2 Manual Recovery";
	guiTag = "rules slayer_ctf respawn";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "CTF";
	title = "Dropped Flag Respawn Time";
	defaultValue = 10;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.CTF_flagDroppedRespawnTime";
	type = "int";
	int_minValue = 0;
	int_maxValue = 1800;
	guiTag = "rules slayer_ctf respawn";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "CTF";
	title = "Returned Flag Respawn Time";
	defaultValue = 0;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.CTF_flagReturnedRespawnTime";
	type = "int";
	int_minValue = 0;
	int_maxValue = 1800;
	guiTag = "rules slayer_ctf respawn";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "CTF";
	title = "Enable Manual Flag Dropping";
	defaultValue = 1;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.CTF_manualFlagDrop";
	type = "bool";
	guiTag = "advanced";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "CTF";
	title = "Capture Requires Enemy Players";
	defaultValue = 0;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.CTF_requireEnemyPlayers";
	type = "bool";
	guiTag = "rules slayer_ctf mode";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "CTF";
	title = "Flag Type";
	defaultValue = 0;
	permissionLevel = $Slayer::PermissionLevel["Host"];
	variable = "$Pref::Slayer::Server::CTF::FlagType";
	type = "list";
	list_items = $flagTypeList;
	guiTag = "advanced";
	notifyPlayersOnChange = false;
	requiresServerRestart = true;
	doNotReset = true;
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "CTF";
	title = "Max Flag Colors";
	defaultValue = 10;
	permissionLevel = $Slayer::PermissionLevel["Host"];
	variable = "$Pref::Slayer::Server::CTF::MaxFlagColors";
	type = "int";
	int_minValue = 2;
	int_maxValue = 64;
	notifyPlayersOnChange = false;
	requiresServerRestart = true;
	doNotSendToClients = true;
	doNotReset = true;
};

// Restricted Events
// KEY: -1=anyone with edit rights; 0=host; 1=super admin; 2=admin; 3=creator; 4=full trust; 5=build trust
$Slayer::Server::Events::RestrictedEvent__["Player", "DropFlag"] = -1;
if(isFile($Slayer::Server::ConfigDir @ "/restrictedEvents.cs"))
	exec($Slayer::Server::ConfigDir @ "/restrictedEvents.cs");
export("$Slayer::Server::Events::RestrictedEvent__*", $Slayer::Server::ConfigDir @ "/restrictedEvents.cs", false);

// Input Events
registerInputEvent(FxDtsBrick, "onFlagPickedUp", "Self FxDtsBrick\tPlayer Player\tClient GameConnection\tMiniGame MiniGame");
registerInputEvent(FxDtsBrick, "onFlagDropped", "Self FxDtsBrick\tPlayer Player\tClient GameConnection\tMiniGame MiniGame");
registerInputEvent(FxDtsBrick, "onFlagReturned", "Self FxDtsBrick\tPlayer Player\tClient GameConnection\tMiniGame MiniGame");
registerInputEvent(FxDtsBrick, "onFlagRecovered", "Self FxDtsBrick\tPlayer Player\tClient GameConnection\tMiniGame MiniGame");

// Output Events
registerOutputEvent(Player, "DropFlag", "");