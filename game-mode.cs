// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

//Create the game mode template.
new ScriptGroup(Slayer_GameModeTemplateSG)
{
	className = "Slayer_CTF";
	uiName = "Capture the Flag";
	useTeams = true;
};
$Slayer::GameModes::NewClass["CTF"] = "Slayer_CTF";

//Bot navigation prefs.
$Slayer::Server::Bots::Priority["CTF_Flag"] = 500;
$Slayer::Server::Bots::Priority["CTF_Return"] = 1000;

//The selected flag model.
$Slayer::Server::CTF::flagModel = $Pref::Slayer::Server::CTF::FlagType;

function createSlayerCTFDatablocks()
{
	//FLAG SPAWN BRICK
	datablock fxDtsBrickData(brickSlyrCTFFlagData : brick2x2Data)
	{
		uiName = "Flag Spawn";
		category = $Slayer::Server::Bricks::Category;
		subCategory = $Slayer::Server::Bricks::SubCategory @ "CTF";

		indestructable = 1;

		isSlyrBrick = 1;
		slyrType = "CTF_Flag";

		setOnceOnly = 0;
	};

	//FLAG RETURN BRICK
	datablock fxDtsBrickData(brickSlyrCTFFlagReturnData : brick6x6FData)
	{
		uiName = "Flag Return Point";
		category = $Slayer::Server::Bricks::Category;
		subCategory = $Slayer::Server::Bricks::SubCategory @ "CTF";

		indestructable = 1;

		isSlyrBrick = 1;
		slyrType = "CTF_FlagReturn";
	};

	//FLAG RETURN TRIGGER
	datablock TriggerData(slyrCTF_flagReturnTriggerData)
	{
		tickPeriodMS = 150;
	};

	//FLAGS
	for(%i = 0; %i < $Pref::Slayer::Server::CTF::MaxFlagColors; %i ++)
	{
		%color = getColorIDTable(%i);
		datablock ItemData(flagItem)
		{
			className = slyrCTF_FlagItem;

			shapeFile = $Slayer::Server::CTF::flagShapeFile[$Slayer::Server::CTF::flagModel];
			uiName = "";

			doColorShift = true;
			colorShiftColor = %color;
			colorID = %i;
		};
		flagItem.setName("slyrCTF_Flag" @ %i @ "Item");

		datablock ShapeBaseImageData(flagImage)
		{
			className = slyrCTF_FlagImage;
			// class = slyrCTF_FlagImage;

			shapeFile = $Slayer::Server::CTF::flagShapeFile[$Slayer::Server::CTF::flagModel];
			emap = true;
			mountPoint = $Slayer::Server::CTF::flagMountPoint[$Slayer::Server::CTF::flagModel];
			offset = $Slayer::Server::CTF::flagOffset[$Slayer::Server::CTF::flagModel];
			eyeOffset = "0 0 10";

			rotate = true;
			rotation = $Slayer::Server::CTF::flagRotation[$Slayer::Server::CTF::flagModel];

			armReady = false;

			doColorShift = true;
			colorShiftColor = %color;
			colorID = %i;

			hasLight = $Slayer::Server::CTF::flagHasLight[$Slayer::Server::CTF::flagModel];
			lightType = $Slayer::Server::CTF::flagLightType[$Slayer::Server::CTF::flagModel];
			lightColor = %color;
			lightTime = $Slayer::Server::CTF::flagLightTime[$Slayer::Server::CTF::flagModel];
			lightRadius = $Slayer::Server::CTF::flagLightRadius[$Slayer::Server::CTF::flagModel];

			stateName[0] = $Slayer::Server::CTF::flagStateName[0,$Slayer::Server::CTF::flagModel];
			stateTransitionOnTimeout[0] = $Slayer::Server::CTF::flagStateTransitionOnTimeout[0,$Slayer::Server::CTF::flagModel];
			stateTimeoutValue[0] = $Slayer::Server::CTF::flagStateTimeoutValue[0,$Slayer::Server::CTF::flagModel];
			stateSequence[0] = $Slayer::Server::CTF::flagStateSequence[0,$Slayer::Server::CTF::flagModel];
		};
		flagImage.setName("slyrCTF_Flag" @ %i @ "Image");
	}
}
if(!isObject(brickSlyrCTFFlagData))
	createSlayerCTFDatablocks();

function Slayer_CTF::onAdd(%this)
{
	%this.bricks = new SimSet();
	%this.flags = new SimSet();
}

function Slayer_CTF::onRemove(%this)
{
	if(isObject(%this.bricks))
		%this.bricks.delete();
	if(isObject(%this.flags))
		%this.flags.delete();
}

function Slayer_CTF::onGameModeStart(%this)
{
	%this.resetFlags(false);
}

function Slayer_CTF::onGameModeEnd(%this)
{
	%this.resetFlags(true);
	for(%i = 0; %i < %this.minigame.teams.getCount(); %i ++)
	{
		%team = %this.minigame.teams.getObject(%i);
		%team.CTF_numFlagReturns = "";
	}
}

function Slayer_CTF::onMiniGameBrickAdded(%this, %brick, %type)
{
	%client = %brick.getGroup().client;
	%color = %brick.getColorID();
	%db = %brick.getDatablock();
	switch$(%type)
	{
		case "CTF_Flag":
			%this.bricks.add(%brick);
			if(!isObject(%brick.ctfTrigger))
				%brick.ctfTrigger = %brick.createTrigger(slyrCTF_flagReturnTriggerData);
			%brick.resetFlag();
			%teams = %this.minigame.teams.getTeamsFromColor(%color,"COM",1);
			if(%teams !$= "")
				return "<just:center>\c5" @ %db.uiName SPC "set for" SPC %teams @ "\c5.";

		case "CTF_FlagReturn":
			%this.bricks.add(%brick);
			if(!isObject(%brick.ctfTrigger))
				%brick.ctfTrigger = %brick.createTrigger(slyrCTF_flagReturnTriggerData);
			%teams = %this.minigame.teams.getTeamsFromColor(%color,"COM",1);
			if(%teams !$= "")
				return "<just:center>\c5" @ %db.uiName SPC "set for" SPC %teams @ "\c5.";
	}
	return 0;
}

function Slayer_CTF::onMiniGameBrickRemoved(%this, %brick, %type)
{
	if(isObject(%brick.ctfTrigger))
		%brick.ctfTrigger.delete();
}

function Slayer_CTF::preMinigameReset(%this, %client)
{
	%this.resetFlags(false);
	for(%i = %this.minigame.teams.getCount() - 1; %i >= 0; %i --)
	{
		%team = %this.minigame.teams.getObject(%i);
		%team.CTF_numFlagReturns = "";
		%team.CTF_numFlagPickups = "";
	}
	for(%i = %this.minigame.numMembers - 1; %i >= 0; %i --)
	{
		%cl = %this.minigame.member[%i];
		%cl.CTF_numFlagReturns = "";
		%cl.CTF_numFlagPickups = "";
	}
}

function Slayer_CTF::prePlayerDeath(%this, %client, %obj, %killer, %type, %area)
{
	if(isObject(%client.player))
		%client.player.dropFlag();
}

function Slayer_CTF::onClientLeaveGame(%this, %client)
{
	if(isObject(%client.player))
		%client.player.dropFlag();
}

function Slayer_CTF::onClientLeaveTeam(%this, %team, %client)
{
	if(isObject(%client.player))
		%client.player.dropFlag();
}

function Slayer_CTF::onClientDisplayRules(%this, %client)
{
	if(%this.minigame.CTF_flagReturnsToWin > 0)
	{
		%cap = (%this.minigame.CTF_flagReturnsToWin == 1 ? "capture" : "captures");
		%flag = (%this.minigame.CTF_flagReturnsToWin == 1 ? "flag" : "flags");
		messageClient(%client,'',"\c5 +\c3" SPC %this.minigame.CTF_flagReturnsToWin SPC "\c5flag" SPC %cap SPC "- The first team to capture\c3" SPC %this.minigame.CTF_flagReturnsToWin SPC "\c5" @ %flag SPC "wins.");
	}
}

function Slayer_CTF::scoreListInit(%this, %header, %var1, %var2, %var3, %var4, %var5, %var6, %var7, %var8, %var9)
{
	%header = '<color:ffffff><tab:100,200,325,450><font:arial:16>%1<h2>%2\t%3\t%4\t%5\t%6</h2><br>';
	%var4 = "Flag Pick-ups";
	%var5 = "Flag Returns";
	return true TAB %header TAB %var1 TAB %var2 TAB %var3 TAB %var4 TAB %var5 TAB %var6;
}

function Slayer_CTF::scoreListAdd(%this, %obj, %line, %var1, %var2, %var3, %var4, %var5, %var6, %var7, %var8, %var9)
{
	%var4 = %obj.CTF_numFlagPickups;
	%var5 = %obj.CTF_numFlagReturns;
	return true TAB %line TAB %var1 TAB %var2 TAB %var3 TAB %var4 TAB %var5 TAB %var6;
}

function Slayer_CTF::isBrickNeutral(%this, %brick)
{
	%color = %brick.getColorID();
	return %this.minigame.teams.isNeutralColor(%color);
}

function Slayer_CTF::getFlagDisplayName(%this, %brick)
{
	%color = %brick.getColorID();
	%name = %this.minigame.teams.getTeamsFromColor(%color, "COM", 1);
	if(%name $= "")
	{
		%brickName = trim(strReplace(%brick.getName(),"_"," "));
		%name = (%brickName $= "" ? "Neutral" : %brickName);
		%name = "<sPush><color:" @ Slayer_Support::RgbToHex(getColorIDTable(%color)) @ ">" @ %name @ "<sPop>";
	}
	return %name;
}

function Slayer_CTF::isFlagAtHome(%this, %color)
{
	%count = %this.bricks.getCount();
	for(%i = 0; %i < %count; %i ++)
	{
		%br = %this.bricks.getObject(%i);
		if(minigameCanUse(%this.minigame, %br) && %br.getDatablock().slyrType $= "CTF_Flag")
		{
			if(%br.getTeamControl() == %color)
			{
				%item = %br.item;
				if(isObject(%item) && %item.getDatablock().className $= "slyrCTF_FlagItem")
					return 1;
				if(!%isFlagSpawn)
					%isFlagSpawn = true;
			}
		}
	}

	if(%isFlagSpawn)
		return 0;
	else
		return -1;
}

function Slayer_CTF::onFlagReturn(%this, %client, %team, %brick, %flag)
{
	%player = %client.player;
	%points = %this.minigame.CTF_points_Flag;
	%teamNames = %this.getFlagDisplayName(%player.flagSpawn);

	%client.incScore(%points);

	if(%points > 0)
		%msg = %team.getColorHex() @ %client.getPlayerName() SPC
			"<color:ff00ff>returned the" SPC %teamNames SPC "flag for\c3" SPC
			%points SPC (%points == 1 ? "\c5point." : "\c5points.");
	else
		%msg = %team.getColorHex() @ %client.getPlayerName() SPC
			"<color:ff00ff>returned the" SPC %teamNames SPC "flag.";
	if(%this.minigame.CTF_flagReturnsToWin > 0)
	{
		%remain = %this.minigame.CTF_flagReturnsToWin - %team.CTF_numFlagReturns - 1;
		if(%remain > 0)
		{
			%cap = (%remain == 1 ? "capture" : "captures");
			%msg = %msg SPC %team.getColoredName() SPC "\c5needs\c3" SPC
				%remain SPC "\c5flag" SPC %cap SPC "to win.";
		}
	}

	%respawnTime = %this.minigame.CTF_flagReturnedRespawnTime;
	if(%respawnTime > 0)
	{
		%this.flagRespawnTick(%player.flagSpawn, 0, 0);
		%msg = %msg SPC "The flag will respawn in\c3" SPC %respawnTime SPC
			(%respawnTime == 1 ? "\c5second." : "\c5seconds.");
	}
	else
	{
		%player.flagSpawn.resetFlag();
	}

	%this.minigame.messageAll('',%msg);

	%team.CTF_numFlagReturns ++;
	%client.CTF_numFlagReturns ++;
	
	// onFlagReturned input event
	$InputTarget_["Self"] = %player.flagSpawn;
	$InputTarget_["Player"] = %player;
	$InputTarget_["Client"] = %client;
	$InputTarget_["MiniGame"] = %this.minigame;
	%player.flagSpawn.processInputEvent("onFlagReturned", %client);

	%player.allowFlagRemoval = 1;
	%player.unMountImage($Slayer::Server::CTF::flagImageSlot,1);
	%player.flagSpawn = "";
	
	if(%team.CTF_numFlagReturns >= %this.minigame.CTF_flagReturnsToWin && %this.minigame.CTF_flagReturnsToWin > 0)
		%this.minigame.endRound(%team);
}

function Slayer_CTF::onFlagRecovery(%this, %client, %team, %brick, %flag)
{
	%teamNames = %this.getFlagDisplayName(%brick);

	if(isObject(%flag))
		%flag.delete();
	%brick.resetFlag();

	%points = %this.minigame.CTF_points_FlagRecovery;
	%client.incScore(%points);

	if(%points > 0)
		%this.minigame.messageAll('',%team.getColorHex() @ %client.getPlayerName() SPC "<color:ff00ff>recovered the" SPC %teamNames SPC "flag for\c3" SPC %points SPC (%points == 1 ? "\c5point." : "\c5points."));
	else
		%this.minigame.messageAll('',%team.getColorHex() @ %client.getPlayerName() SPC "<color:ff00ff>recovered the" SPC %teamNames SPC "flag.");

	// onFlagRecovered input event
	$InputTarget_["Self"] = %brick;
	$InputTarget_["Player"] = %client.player;
	$InputTarget_["Client"] = %client;
	$InputTarget_["MiniGame"] = %this.minigame;
	%brick.processInputEvent("onFlagRecovered", %client);
}

function Slayer_CTF::onFlagPickup(%this, %client, %team, %brick, %flag)
{
	%flagDB = %flag.getDatablock();
	%player = %client.player;
	%teamNames = %this.getFlagDisplayName(%brick);

	cancel(%this.minigame.CTF_flagRespawnSched[%flagDB.colorID]);

	%image = "slyrCTF_Flag" @ %flagDB.colorID @ "Image";
	%player.mountImage(%image.getID(),$Slayer::Server::CTF::flagImageSlot);
	%player.flagSpawn = %brick;

	%brick.setItem(0);

	if(isObject(%flag))
	{
		if(!%flag.dropped)
		{
			%team.CTF_numFlagPickups ++;
			%client.CTF_numFlagPickups ++;
		}

		%flag.delete();
	}

	%this.minigame.messageAll('',%team.getColorHex() @ %client.getPlayerName() SPC "<color:ff00ff>picked up the" SPC %teamNames SPC "\c5flag.");

	if(%this.minigame.CTF_manualFlagDrop)
		%client.bottomPrint("<just:center>\c5Type \c3/dropFlag \c5to drop the flag." NL
			"<font:Palatino Linotype:18>\c5(Alternatively, use the \c3Drop Tool \c5key when your tools are put away.)",4,true);

	if(%client.isSlayerBot)
	{
		%count = %this.bricks.getCount();
		for(%i = 0; %i < %count; %i ++)
		{
			%br = %this.bricks.getObject(%i);
			%type = %br.getDatablock().slyrType;
			if((%type $= "CTF_Flag" || %type $= "CTF_FlagReturn") && minigameCanUse(%this.minigame, %br))
			{
				if(%br.getTeamControl() == %team.color)
				{
					%client.addObjective(%br, $Slayer::Server::Bots::Priority["CTF_Return"]);
					%client.schedule(0, "goToNextObjective", true);
					break;
				}
			}
		}
	}

	// onFlagPickedUp input event
	$InputTarget_["Self"] = %brick;
	$InputTarget_["Player"] = %player;
	$InputTarget_["Client"] = %client;
	$InputTarget_["MiniGame"] = %this.minigame;
	%brick.processInputEvent("onFlagPickedUp", %client);	
}

function Slayer_CTF::onFlagDrop(%this, %client,  %team, %brick, %flag)
{
	%teamNames = %this.getFlagDisplayName(%brick);
	%this.minigame.messageAll('',%team.getColorHex() @ %client.getPlayerName() SPC "<color:ff00ff>dropped the" SPC %teamNames SPC "\c5flag.");

	// onFlagDropped input event
	$InputTarget_["Self"] = %brick;
	$InputTarget_["Player"] = %client.player;
	$InputTarget_["Client"] = %client;
	$InputTarget_["MiniGame"] = %this.minigame;
	%brick.processInputEvent("onFlagDropped", %client);
}

function Slayer_CTF::flagRespawnTick(%this, %brick, %item, %ticks)
{
	if(!isObject(%this.minigame))
		return;
	if(!isObject(%brick))
		return;
	%color = %brick.getColorID();
	
	%dropped = isObject(%item);
	if(%dropped)
	{
		%time = %this.minigame.CTF_flagDroppedRespawnTime;
	}
	else
	{
		%time = %this.minigame.CTF_flagReturnedRespawnTime;
	}
	
	cancel(%this.minigame.CTF_flagRespawnSched[%color]);
	if(%ticks >= %time)
	{
		%teamNames = %this.getFlagDisplayName(%brick);
		%this.minigame.messageAll('',"<color:ff00ff>The" SPC %teamNames SPC
			"flag was respawned.");
		if(%dropped)
		{
			%item.delete();
		}
		%brick.resetFlag();
	}
	else
	{
		if(%dropped)
		{
			%item.setShapeNameColor(getColorIDTable(%color));
			%item.setShapeName(%time - %ticks);
		}
		%this.minigame.CTF_flagRespawnSched[%color] = %this.scheduleNoQuota(
			1000, "flagRespawnTick", %brick, %item, %ticks ++);
	}
}

function Slayer_CTF::resetFlags(%this, %doNotRespawn)
{
	for(%i = %this.flags.getCount() - 1; %i >= 0; %i --)
	{
		%f = %this.flags.getObject(%i);
		if(minigameCanUse(%this, %f))
			%f.delete();
	}

	for(%i = 0; %i < %this.bricks.getCount(); %i ++)
	{
		%br = %this.bricks.getObject(%i);
		if(%br.getDatablock().slyrType $= "CTF_Flag" && minigameCanUse(%this, %br))
		{
			if(%doNotRespawn)
				%br.setItem(0);
			else
				%br.resetFlag();
		}
	}
}

function Slayer_CTF::assignBotObjectives(%this, %ai)
{
	%team = %ai.slyrTeam;
	if(isObject(%team))
	{
		//flag spawns
		%count = %this.bricks.getCount();
		for(%i = 0; %i < %count; %i ++)
		{
			%br = %this.bricks.getObject(%i);
			if(%br.getDatablock().slyrType !$= "CTF_Flag" || !isObject(%br.item))
				continue;
			%blid = %br.getGroup().bl_id;
			if(%canUse[%blid] $= "")
				%canUse[%blid] = miniGameCanUse(%this.minigame, %br);
			if(%canUse[%blid])
			{
				if(%br.getTeamControl() != %team.color)
				{
					%ai.addObjective(%br, $Slayer::Server::Bots::Priority["CTF_Flag"], "CTF_Flag");
				}
			}
		}
	}
}

function Slayer_onBotObjectiveReached_CTF_Flag(%minigame, %ai, %objective)
{
	talk("BOT" SPC %ai SPC "JUST REACHED FLAG" SPC %objective);
	return -1;
}

function Slayer_CTF_getClosestCTFColor(%rgba)
{
	%prevdist = 100000;
	%colorMatch = 0;
	for(%i=0; %i < $Pref::Slayer::Server::CTF::MaxFlagColors; %i++)
	{
		%color = getColorIDTable(%i);
		if(vectorDist(%rgba,getWords(%color,0,2)) < %prevdist && getWord(%rgba,3) - getWord(%color,3) < 0.3 && getWord(%rgba,3) - getWord(%color,3) > -0.3)
		{
			%prevdist = vectorDist(%rgba,%color);
			%colormatch = %i;
		}
	}
	return %colormatch;
}

function FxDtsBrick::resetFlag(%this)
{
	if(%this.getDatablock().slyrType !$= "CTF_Flag")
		return;

	%color = %this.getColorID();
	%flagColor = Slayer_CTF_getClosestCTFColor(getColorIDTable(%color));

	%item = "slyrCTF_Flag" @ %flagColor @ "Item";
	%item = %item.getID();
	%item.uiName = " "; //items without a name don't spawn
	%this.setItem(%item);
	%item.uiName = "";
}

function Player::getFlagImage(%this)
{
	return %this.getMountedImage($Slayer::Server::CTF::flagImageSlot);
}

function Player::isCarryingFlag(%this)
{
	%image = %this.getMountedImage($Slayer::Server::CTF::flagImageSlot);
	return %image.className $= "slyrCTF_FlagImage";
}

function Player::dropFlag(%this)
{
	%image = %this.getFlagImage();
	if(!isObject(%image))
		return;

	%client = %this.client;

	%mini = getMinigameFromObject(%client);
	if(!isSlayerMinigame(%mini))
		return;
	if(%mini.gameMode.class !$= "Slayer_CTF")
		return;

	%team = %client.getTeam();
	if(!isObject(%team))
		return;

	%brick = %this.flagSpawn;
	if(!isObject(%brick))
		return;

	%respawn = %mini.CTF_flagDroppedRespawnTime;
	if(%respawn <= 0)
		%brick.resetFlag();
	else
	{
		%eyeVect = %this.getEyeVector();
		%pos = getWords(%this.getEyeTransform(),0,2);
		%endPos = vectorAdd(%pos,vectorScale(%eyeVect,1.1));
		%raycast = containerRayCast(%pos,%endPos,$TypeMasks::fxBrickObjectType | $TypeMasks::TerrainObjectType);

		if(!isObject(getWord(%raycast,0)))
			%pos = %endPos;

		%item = new Item()
		{
			dataBlock = "slyrCTF_Flag" @ %image.colorID @ "Item";

			spawnTime = getSimTime();

			position = %pos;
			spawnBrick = %brick;

			dropped = true;
			canPickup = true;
		};
		missionCleanup.add(%item);
		%mini.gameMode.flagRespawnTick(%brick, %item,0);

		%item.setVelocity(vectorAdd(%item.getVelocity(),vectorScale(%eyeVect,5)));
	}

	%this.allowFlagRemoval = 1;
	%this.unMountImage($Slayer::Server::CTF::flagImageSlot);
	%this.flagSpawn = "";

	%mini.gameMode.onFlagDrop(%client,%team,%brick,%item);
}

function slyrCTF_FlagItem::onAdd(%this,%obj)
{
	for(%i = $Slayer::Server.minigames.getCount() - 1; %i >= 0; %i --)
	{
		%mini = $Slayer::Server.minigames.getObject(%i);
		if(%mini.gameMode.class $= "Slayer_CTF")
		{
			%mini.gameMode.flags.add(%obj);
		}
	}

	if($Slayer::Server::CTF::flagIdleAnimation[$Slayer::Server::CTF::flagModel] !$= "")
		%obj.playThread(0,$Slayer::Server::CTF::flagIdleAnimation[$Slayer::Server::CTF::flagModel]);

	//spinning flags
	if(!%obj.dropped && $Slayer::Server::CTF::flagRotate[$Slayer::Server::CTF::flagModel])
		%obj.rotate = 1;
}

function slyrCTF_FlagItem::onPickUp(%this,%flag,%player,%a)
{
	if(getSimTime() - %flag.spawnTime < 200)
		return;

	%client = %player.client;
	%image = %player.getMountedImage($Slayer::Server::CTF::flagImageSlot);
	%mini = getMinigameFromObject(%client);

	if(!isSlayerMinigame(%mini) || %mini.gameMode.class !$= "Slayer_CTF" || %mini.isResetting())
		return;

	if(!minigameCanUse(%this, %flag))
	{
		%client.centerPrint("That flag is not part of your minigame.",2);
		return;
	}

	%team = %client.getTeam();
	if(!isObject(%team))
		return;

	%brick = %flag.spawnBrick;
	if(!isObject(%brick))
		return;

	%teamNames = %mini.gameMode.getFlagDisplayName(%brick);

	%color = %brick.getTeamControl();

	//TOUCHING OUR OWN FLAG
	if(%color == %team.color)
	{
		//FLAG RECOVERY
		if(%flag.dropped)
		{
			if(%mini.CTF_flagRecovery == 1)
				%mini.gameMode.onFlagRecovery(%client,%team,%brick,%flag);
			else if(%mini.CTF_flagRecovery == 2 && %image.className !$= "slyrCTF_FlagImage")
				%mini.gameMode.onFlagPickup(%client,%team,%brick,%flag);
		}
	}
	//FLAG CAPTURE
	else if(%image.className !$= "slyrCTF_FlagImage")
	{
		//LOCKED BRICK?
		if(%brick.isLocked[%team.color] && %color != %team.color)
		{
			%client.bottomPrint("<just:center>\c5This flag is locked. You cannot capture it yet.",1);
			return;
		}

		//NEUTRAL FLAG?
		%neutral = %mini.gameMode.isBrickNeutral(%brick);
		if(%neutral && %mini.CTF_neutralFlags == 0)
		{
			%client.bottomPrint("<just:center>\c5You may not pick up neutral flags.",1);
			return;
		}
		else if(!%neutral && %mini.CTF_neutralFlags == 2)
		{
			%client.bottomPrint("<just:center>\c5You may only pick up neutral flags.",1);
			return;
		}

		if(!%neutral && %mini.CTF_requireEnemyPlayers)
		{
			%hasMembers = false;
			for(%i = 0; %i < %mini.teams.getCount(); %i ++)
			{
				%t = %mini.teams.getObject(%i);
				if(%t.color == %color && %t.numMembers > 0)
				{
					%hasMembers = true;
					break;
				}
			}

			if(!%hasMembers)
			{
				%client.bottomPrint("<just:center><color:ff00ff>You cannot capture this flag because nobody is on" SPC %teamNames @ ".",1);
				return;
			}
		}

		%mini.gameMode.onFlagPickup(%client,%team,%brick,%flag);
	}
}

function slyrCTF_flagReturnTriggerData::onEnterTrigger(%this,%trigger,%player)
{
	if(!(%player.getType() & $TypeMasks::PlayerObjectType) || %player.dataBlock.rideable)
		return;

	%client = %player.client;
	if(!isObject(%client))
		return;

	Slayer_Support::Debug(3,"CTF Flag Trigger Enter",%trigger TAB %player TAB %client);

	//ARE WE HOLDING A FLAG?
	if(!%player.isCarryingFlag())
		return;

	//VALID MINIGAME?
	%mini = getMinigameFromObject(%client);
	%team = %client.getTeam();
	if(!isSlayerMinigame(%mini) || !isObject(%team) || %mini.gameMode.class !$= "Slayer_CTF")
		return;
	if(%mini.isResetting())
		return;

	//VALID FLAG RETURN BRICK?
	%brick = %trigger.brick;
	if(!minigameCanUse(%this, %brick))
		return;
	%datablock = %brick.getDatablock();
	%color = %brick.getTeamControl();
	%neutral = %mini.teams.isNeutralColor(%color);
	%slyrType = %datablock.slyrType;

	%playerFlagSpawn = %player.flagSpawn;
	if(!isObject(%playerFlagSpawn))
	{
		Slayer_Support::Error("slyrCTF_flagReturnTriggerData::onEnterTrigger","Flag spawn not found!");
		return;
	}

	//WE CAN RETURN FLAGS HERE
	if(%color == %team.color || (%neutral && %slyrType $= "CTF_FlagReturn"))
	{
		//LOCKED BRICK?
		if(%brick.isLocked[%team.color])
		{
			%client.bottomPrint("<just:center>\c5This" SPC %datablock.uiName SPC "is locked. You cannot return flags here yet.",4);
			return;
		}

		%flagColor = %playerFlagSpawn.getColorID();

		//IT'S OUR FLAG
		if(%flagColor == %team.color)
		{
			if(%slyrType $= "CTF_Flag")
			{
				%item = %brick.item;
				if(!isObject(%item) || %item.getDatablock().className !$= "slyrCTF_FlagItem")
				{
					//FLAG RECOVERY
					%player.allowFlagRemoval = 1;
					%player.unMountImage($Slayer::Server::CTF::flagImageSlot);
					%player.flagSpawn = "";
					%mini.gameMode.onFlagRecovery(%client,%team,%brick,0);
				}
			}
		}
		//ENEMY FLAG
		else
		{
			//ONLY ALLOW RETURN AT FLAG RETURN BRICKS?
			if(%mini.CTF_flagReturnOnlyAtReturnBrick && %slyrType !$= "CTF_FlagReturn")
			{
				%client.bottomPrint("<just:center>\c5You must return the flag to a Flag Return Point.",4);
				return;
			}

			//RETURNING TO SAME COLOR?
			if(%flagColor == %color && %neutral)
			{
				%client.bottomPrint("<just:center>\c5You cannot return a flag to a Flag Return Point of the same color.",4);
				return;
			}

			//WE CAN'T RETURN IT IF WE DON'T HAVE OUR OWN FLAG
			if(!%mini.CTF_returnWithoutOwn)
			{
				if(%slyrType $= "CTF_Flag")
				{
					%item = %brick.item;
					if(!isObject(%item) || %item.getDatablock().className !$= "slyrCTF_FlagItem")
						%noFlag = 1;
				}
				else
				{
					if(!%mini.gameMode.isFlagAtHome(%team.color))
						%noFlag = 1;
				}
			}
			if(%noFlag)
			{
				messageClient(%client,'',"\c5You cannot return this flag while your flag is missing.");
				return;
			}

			//FLAG RETURN
			%mini.gameMode.onFlagReturn(%client,%team,%brick,0);
		}
	}
}


//This controls what bots do when they reach a flag.
//@param	mini	The bot's minigame.
//@param	ai	The AiConnection.
//@param	objective	The objective that was reached.
function Slayer_onBotObjectiveReached_CTFFlag(%this, %ai,%objective)
{
	%bot = %ai.player;
	if(!isObject(%bot))
		return;

	%team = %ai.getTeam();

	if(!isObject(%team) || %objective.getTeamControl() == %team)
		return;
	else
	{
		%bot.hClearMovement();

		if(getRandom(0,2))
			%bot.hCrouch(1500 + getRandom(1000,6000));

		//returning -1 prevents the bot from
		//continuing to the next objective
		return -1;
	}
}

//Allows the client to manually drop a flag.
function serverCmdDropFlag(%client)
{
	if(%client.isSpamming())
		return;
	%minigame = getMinigameFromObject(%client);
	if(!%minigame.CTF_manualFlagDrop)
		return;
	if(isObject(%client.player))
		%client.player.dropFlag();
}

package Slayer_Gamemode_CTF
{
	function Player::mountImage(%this,%image,%slot,%a,%b)
	{
		%curImage = %this.getMountedImage(%slot);
		if(isObject(%curImage) && %curImage.className $= "SlyrCTF_FlagImage")
			return;

		%parent = parent::mountImage(%this,%image,%slot,%a,%b);

		return %parent;
	}

	function Player::unMountImage(%this,%slot)
	{
		%curImage = %this.getMountedImage(%slot);
		if(!%this.allowFlagRemoval && isObject(%curImage) && %curImage.className $= "SlyrCTF_FlagImage")
			return;

		%this.allowFlagRemoval = 0;

		return parent::unMountImage(%this,%slot);
	}

	function serverCmdDropTool(%client, %slot)
	{
		%minigame = getMinigameFromObject(%client);
		if(%client.player.currTool == -1 && %client.player.isCarryingFlag() &&
			%minigame.CTF_manualFlagDrop)
		{
			%client.player.dropFlag();
		}
		else
		{
			parent::serverCmdDropTool(%client, %slot);
		}
	}

	function serverCmdSetWrenchData(%client,%info)
	{
		%brick = %client.wrenchBrick;
		if(isObject(%brick.item))
			%itemDB = %brick.item.getDatablock();
		parent::serverCmdSetWrenchData(%client,%info);
		if(%brick.getDatablock().slyrType $= "CTF_Flag")
		{
			if(isObject(%itemDB) && %itemDB.className $= "slyrCTF_FlagItem")
				%brick.resetFlag();
		}
	}
};
activatePackage(Slayer_Gamemode_CTF);