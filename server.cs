// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

%error = forceRequiredAddon("Gamemode_Slayer");
if(%error == $Error::Addon_NotFound)
	error("ERROR: Gamemode_Slayer_CTF - Required add-on Gamemode_Slayer not found!");
else
{
	$Slayer::Server::CTF::numFlags = 0;
	$Slayer::Server::CTF::flagImageSlot = 3;

	$Slayer::Server::CTF::flagName[$Slayer::Server::CTF::numFlags] = "Standard Flag";
	$Slayer::Server::CTF::flagShapeFile[$Slayer::Server::CTF::numFlags] = "./Flags/flag.dts";
	$Slayer::Server::CTF::flagMountPoint[$Slayer::Server::CTF::numFlags] = $BackSlot;
	$Slayer::Server::CTF::flagRotation[$Slayer::Server::CTF::numFlags] = eulerToMatrix("30 0 270");
	$Slayer::Server::CTF::flagOffset[$Slayer::Server::CTF::numFlags] = "0.3 -0.35 -.6";
	$Slayer::Server::CTF::flagRotate[$Slayer::Server::CTF::numFlags] = 0;
	$Slayer::Server::CTF::flagHasLight[$Slayer::Server::CTF::numFlags] = 1;
	$Slayer::Server::CTF::flagLightType[$Slayer::Server::CTF::numFlags] = "ConstantLight";
	$Slayer::Server::CTF::flagLightTime[$Slayer::Server::CTF::numFlags] = 100000;
	$Slayer::Server::CTF::flagLightRadius[$Slayer::Server::CTF::numFlags] = 20;
	$Slayer::Server::CTF::flagStateName[0,$Slayer::Server::CTF::numFlags] = "Idle";
	$Slayer::Server::CTF::flagStateTransitionOnTimeout[0,$Slayer::Server::CTF::numFlags] = "Idle";
	$Slayer::Server::CTF::flagStateTimeoutValue[0,$Slayer::Server::CTF::numFlags] = 2;
	$Slayer::Server::CTF::flagStateSequence[0,$Slayer::Server::CTF::numFlags] = "root";
	$Slayer::Server::CTF::flagIdleAnimation[$Slayer::Server::CTF::numFlags] = "root";
	$Slayer::Server::CTF::numFlags ++;

	$Slayer::Server::CTF::flagName[$Slayer::Server::CTF::numFlags] = "Pole Flag";
	$Slayer::Server::CTF::flagShapeFile[$Slayer::Server::CTF::numFlags] = "./Flags/pole.dts";
	$Slayer::Server::CTF::flagMountPoint[$Slayer::Server::CTF::numFlags] = $BackSlot;
	$Slayer::Server::CTF::flagRotation[$Slayer::Server::CTF::numFlags] = eulerToMatrix("30 0 270");
	$Slayer::Server::CTF::flagOffset[$Slayer::Server::CTF::numFlags] = "0.3 -0.35 -.6";
	$Slayer::Server::CTF::flagRotate[$Slayer::Server::CTF::numFlags] = 1;
	$Slayer::Server::CTF::numFlags ++;

	$Slayer::Server::CTF::flagName[$Slayer::Server::CTF::numFlags] = "Briefcase Flag";
	$Slayer::Server::CTF::flagShapeFile[$Slayer::Server::CTF::numFlags] = "./Flags/briefcase.dts";
	$Slayer::Server::CTF::flagMountPoint[$Slayer::Server::CTF::numFlags] = $BackSlot;
	$Slayer::Server::CTF::flagRotation[$Slayer::Server::CTF::numFlags] = eulerToMatrix("180 135 0");
	$Slayer::Server::CTF::flagOffset[$Slayer::Server::CTF::numFlags] = "0 -0.36 -0.55";
	$Slayer::Server::CTF::flagRotate[$Slayer::Server::CTF::numFlags] = 0;
	$Slayer::Server::CTF::numFlags ++;

	$Slayer::Server::CTF::flagName[$Slayer::Server::CTF::numFlags] = "Deathball Skull";
	$Slayer::Server::CTF::flagShapeFile[$Slayer::Server::CTF::numFlags] = "./Flags/deathball.dts";
	$Slayer::Server::CTF::flagMountPoint[$Slayer::Server::CTF::numFlags] = $LeftHandSlot;
	$Slayer::Server::CTF::flagRotation[$Slayer::Server::CTF::numFlags] = eulerToMatrix("0 90 0");
	$Slayer::Server::CTF::flagOffset[$Slayer::Server::CTF::numFlags] = "0.05 0 0.2";
	$Slayer::Server::CTF::flagRotate[$Slayer::Server::CTF::numFlags] = 0;
	$Slayer::Server::CTF::numFlags ++;

	$Slayer::Server::CTF::flagName[$Slayer::Server::CTF::numFlags] = "Fumbi Ball";
	$Slayer::Server::CTF::flagShapeFile[$Slayer::Server::CTF::numFlags] = "./Flags/fumbi.dts";
	$Slayer::Server::CTF::flagMountPoint[$Slayer::Server::CTF::numFlags] = $HeadSlot;
	$Slayer::Server::CTF::flagRotation[$Slayer::Server::CTF::numFlags] = eulerToMatrix("0 0 0");
	$Slayer::Server::CTF::flagOffset[$Slayer::Server::CTF::numFlags] = "0 0 0";
	$Slayer::Server::CTF::flagRotate[$Slayer::Server::CTF::numFlags] = 0;
	$Slayer::Server::CTF::numFlags ++;

	$flagTypeList = "0" SPC $Slayer::Server::CTF::flagName[0];
	for($index = 1; $index < $Slayer::Server::CTF::numFlags; $index ++)
	{
		$flagTypeList = $flagTypeList NL $index SPC $Slayer::Server::CTF::flagName[$index];
	}

	exec("./preferences.cs");
	exec("./game-mode.cs");
}