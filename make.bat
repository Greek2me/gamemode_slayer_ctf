@echo off
::#########
:: Slayer windows zip creator
::#########
:: This file is run to create a zip file in Windows
::
:: Requirements:
:: 7zip (Windows does not include any native form to create a zip file)
:: 7zip path need to be placed in Path environment variable

:: GameMode_Slayer
set ZIP_NAME=Gamemode_Slayer_CTF.zip
7z a -mx9 -tzip -r .\%ZIP_NAME% . -x!*.bat -x!*.rst -x!*.markdown -x!*.*~ -x!*.mkdn -x!*.md -x!.* -x!*.zip
